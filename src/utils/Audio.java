package utils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    private Audio(String soundPath) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(soundPath));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) { System.err.println(e.getMessage()); }
    }

    public Clip getClip() {
        return clip;
    }

    private void play() {
        clip.start();
    }

    private void stop() {
        clip.stop();
    }

    public static void playSound(String soundPath) {
        Audio sound = new Audio(soundPath);
        sound.play();
    }
}
