package controller;

import game.Main;
import utils.Audio;
import utils.Res;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private static final int RIGHT_MOVE = 1;
    private static final int LEFT_MOVE = -1;

    private void checkMapEdge() {
        if (Main.scene.getXPos() == -1) {
            Main.scene.setXPos(0);
            Main.scene.setBackground1PosX(-50);
            Main.scene.setBackground2PosX(750);
        }

        if (Main.scene.getXPos() == 4601) {
            Main.scene.setXPos(4600);
            Main.scene.setBackground1PosX(-50);
            Main.scene.setBackground2PosX(750);
        }
    }

    private void setMoving(boolean rightDirection){
        Main.scene.getMario().setMoving(true);
        Main.scene.getMario().setRightDirection(rightDirection);
        if (rightDirection)
            Main.scene.setMovement(RIGHT_MOVE);
        else
            Main.scene.setMovement(LEFT_MOVE);
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.scene.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                checkMapEdge();
                setMoving(true);

            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                checkMapEdge();
                setMoving(false);
            }

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.scene.getMario().setJumping(true);
                Audio.playSound(Res.AUDIO_JUMP);
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.getMario().setMoving(false);
        Main.scene.setMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
