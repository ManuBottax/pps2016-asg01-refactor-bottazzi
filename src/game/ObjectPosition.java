package game;

import static utils.Res.TYPE_BLOCK;
import static utils.Res.TYPE_PIECE;
import static utils.Res.TYPE_TUNNEL;


public enum ObjectPosition {

    Tunnel1(600, 230, TYPE_TUNNEL),
    Tunnel2(1000, 230, TYPE_TUNNEL),
    Tunnel3(1600, 230, TYPE_TUNNEL),
    Tunnel4(1900, 230, TYPE_TUNNEL),
    Tunnel5(2500, 230, TYPE_TUNNEL),
    Tunnel6(3000, 230, TYPE_TUNNEL),
    Tunnel7(3800, 230, TYPE_TUNNEL),
    Tunnel8(4500, 230, TYPE_TUNNEL),

    Block1(400, 180, TYPE_BLOCK),
    Block2(1200, 180, TYPE_BLOCK),
    Block3(1270, 170, TYPE_BLOCK),
    Block4(1340, 160, TYPE_BLOCK),
    Block5(2000, 180, TYPE_BLOCK),
    Block6(2600, 160, TYPE_BLOCK),
    Block7(2650, 180, TYPE_BLOCK),
    Block8(3500, 160, TYPE_BLOCK),
    Block9(3550, 140, TYPE_BLOCK),
    Block10(4000, 170, TYPE_BLOCK),
    Block11(4200, 200, TYPE_BLOCK),
    Block12(4300, 210, TYPE_BLOCK),

    Piece1(402, 145, TYPE_PIECE),
    Piece2(1202, 140, TYPE_PIECE),
    Piece3(1272, 95, TYPE_PIECE),
    Piece4(1342, 40, TYPE_PIECE),
    Piece5(1650, 145, TYPE_PIECE),
    Piece6(2650, 145, TYPE_PIECE),
    Piece7(3000, 135, TYPE_PIECE),
    Piece8(3400, 125, TYPE_PIECE),
    Piece9(4200, 145, TYPE_PIECE),
    Piece10(4600, 40, TYPE_PIECE);

    private int positionX;
    private int positionY;
    private int type;


    public int getX() {
        return this.positionX;
    }

    public int getY() {
        return this.positionY;
    }

    public int getType() {
        return this.type;
    }

    ObjectPosition(int x, int y, int type) {

        this.positionX = x;
        this.positionY = y;
        this.type = type;
    }

}
