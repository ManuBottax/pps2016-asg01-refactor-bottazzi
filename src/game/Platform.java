package game;

import controller.Keyboard;
import model.GameObject;
import model.characters.*;
import model.objects.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JPanel;

import utils.Audio;
import utils.Res;
import utils.Utils;

import static utils.Res.*;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int END_LEVEL_POSITION = 4600;

    private CopyOnWriteArrayList<GameElement> objectList;

    private Mario mario;
    private List<RunnableCharacter> enemyList = new ArrayList<>();
    private int movement;
    private int xPos;
    private int heightLimit;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private Image flagImage;
    private Image castleImage;

    private int background1PosX;
    private int background2PosX;
    private int floorOffsetY;

    public Platform() {
        super();

        initializeBackground();

        initializeCharacter();
        createGameObjects();

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    private void initializeBackground(){
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.castleImage = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.flagImage = Utils.getImage(Res.IMG_FLAG);

        this.background1PosX = BACKGROUND1_X_POS;
        this.background2PosX = BACKGROUND2_X_POS;
        this.floorOffsetY = FLOOR_OFFSET_Y;
    }

    private void initializeCharacter(){
        CharacterFactory characterFactory = new CharacterFactoryImpl();
        this.movement = MOVEMENT;
        this.xPos = X_POS;
        this.heightLimit = HEIGHT_LIMIT;

        this.mario = characterFactory.createMario(300, 245);
        this.enemyList.add(characterFactory.createMushroom(800, 263));
        this.enemyList.add(characterFactory.createTurtle(950, 243));
    }

    private void createGameObjects(){

        GameObjectFactory factory = new GameObjectFactoryImpl();
        this.objectList = new  CopyOnWriteArrayList<>();

        for( ObjectPosition p : ObjectPosition.values()){
            switch (p.getType()){
                case TYPE_TUNNEL:
                    this.objectList.add(factory.createTunnel(p.getX(),p.getY()));
                    break;
                case TYPE_BLOCK:
                    this.objectList.add(factory.createBlock(p.getX(),p.getY()));
                    break;
                case TYPE_PIECE:
                    this.objectList.add(factory.createPiece(p.getX(),p.getY()));
                    break;
            }
        }
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public CopyOnWriteArrayList<GameElement> getObjectList() {
        return objectList;
    }

    public List<RunnableCharacter> getEnemyList() {
        return enemyList;
    }

    public int getMovement() {
        return movement;
    }

    public int getXPos() {
        return xPos;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public void setHeightLimit(int heightLimit){
        this.heightLimit = heightLimit;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public Mario getMario() {
        return this.mario;
    }

    public  void checkContact (){
        for (GameElement o : objectList) {

            for(RunnableCharacter enemy : enemyList) {

                if (enemy.isNearby(o))
                    enemy.contact(o);

                if (enemy.isNearby(o))
                    enemy.contact(o);
            }

            if (this.mario.isNearby(o))
                this.mario.contact(o);

            if (o instanceof Piece && this.mario.contactPiece(o)) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.objectList.remove(o);
            }
        }

        for(RunnableCharacter enemy : enemyList) {

            for( int i = 0; i < enemyList.size(); i++) {

                if (i != enemyList.indexOf(enemy)) {

                    RunnableCharacter target = enemyList.get(i);

                    if (enemy.isNearby(target)) {
                        enemy.contact(enemyList.get(i));
                    }

                    if (this.mario.isNearby(target)) {
                        this.mario.hitEnemy(target);
                    }
                }
            }
        }
    }

    public  void moveFixedObject() {
        if (this.xPos >= 0 && this.xPos <= END_LEVEL_POSITION) {
            for (GameObject object : objectList) {
                object.move();
            }
        }
    }

    public void moveEnemy() {
        for(RunnableCharacter enemy : enemyList)
            enemy.move();
    }

    private void updateBackgroundOnMovement() {

        if (this.getXPos() >= 0 && this.getXPos() <= END_LEVEL_POSITION) {
            this.setXPos(this.getXPos() + this.getMovement());
            this.background1PosX = this.background1PosX - this.getMovement();
            this.background2PosX = this.background2PosX - this.getMovement();
        }

        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        this.checkContact();
        this.updateBackgroundOnMovement();
        this.moveFixedObject();
        this.moveEnemy();

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.getXPos(), 95, null);
        g2.drawImage(this.start, 220 - this.getXPos(), 234, null);

        for (GameElement object : this.getObjectList()) {
            g2.drawImage(object.getObjectImage(), object.getX(),
                    object.getY(), null);
        }

        g2.drawImage(this.flagImage, FLAG_X_POS - this.getXPos(), FLAG_Y_POS, null);
        g2.drawImage(this.castleImage, CASTLE_X_POS - this.getXPos(), CASTLE_Y_POS, null);

        if (this.getMario().isJumping())
            g2.drawImage(this.getMario().jump(), this.getMario().getX(), this.getMario().getY(), null);
        else
            g2.drawImage(this.getMario().walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.getMario().getX(), this.getMario().getY(), null);

        for( RunnableCharacter enemy : this.getEnemyList()) {
            if (enemy instanceof Mushroom) {

                if (enemy.isAlive()) {
                    g2.drawImage(enemy.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), enemy.getX(), enemy.getY(), null);
                }
                else {
                    g2.drawImage(enemy.getDeadImage(), enemy.getX(), enemy.getY() + MUSHROOM_DEAD_OFFSET_Y, null);
                }
            }
            else if (enemy instanceof Turtle){

                if (enemy.isAlive()) {
                    g2.drawImage(enemy.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), enemy.getX(), enemy.getY(), null);
                }
                else {
                    g2.drawImage(enemy.getDeadImage(), enemy.getX(), enemy.getY() + TURTLE_DEAD_OFFSET_Y, null);
                }
            }
        }
    }


}
