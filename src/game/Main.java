package game;

import utils.Audio;
import utils.Res;

import javax.swing.JFrame;
import java.awt.event.WindowEvent;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario 2.0";
    private static final int PAUSE = 3;
    private static final int DEATH_PAUSE = 6000;

    public static Platform scene;

    public static void main(String[] args) {
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.setAlwaysOnTop(true);

        scene = new Platform();
        frame.setContentPane(scene);
        frame.setVisible(true);

        Runnable timerTask = () -> {
            while (true) {
                if (scene.getMario().isAlive()) {
                    Main.scene.repaint();
                    try {
                        Thread.sleep(PAUSE);
                    } catch (InterruptedException e) {
                    }
                }
                else {
                    try {
                        Audio.playSound(Res.AUDIO_DEATH);
                        Thread.sleep(DEATH_PAUSE);

                    } catch (InterruptedException e) {
                    }

                    Thread.currentThread().interrupt();
                    frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                }
            }
        };

        new Thread(timerTask).start();

    }
}
