package model;

/**
 * Created by ManuBottax on 16/03/2017.
 */
public interface GameObject {

    int getWidth();
    int getHeight();

    int getX();
    int getY();

    void move();
}
