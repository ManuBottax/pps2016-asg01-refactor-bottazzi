package model.characters;


public class CharacterFactoryImpl implements CharacterFactory {

    private boolean marioCreated = false;

    @Override
    public RunnableCharacter createMushroom(int xPosition, int yPosition) {
        return new Mushroom(xPosition,yPosition);
    }

    @Override
    public RunnableCharacter createTurtle(int xPosition, int yPosition) {
        return new Turtle(xPosition,yPosition);
    }

    @Override
    public Mario createMario(int xPosition, int yPosition) {
        if (!marioCreated){
            marioCreated = true;
            return new Mario(xPosition, yPosition);
        }
        else{
            return null;
        }
    }
}
