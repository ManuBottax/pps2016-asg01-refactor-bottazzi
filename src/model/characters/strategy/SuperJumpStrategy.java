package model.characters.strategy;


import game.Main;
import model.characters.Mario;

public class SuperJumpStrategy implements JumpingStrategy{

        private static final int JUMPING_LIMIT = 25;

        private int jumpingExtent = 0;

        @Override
        public void performJump( Mario target ) {

            this.jumpingExtent++;

            if (this.jumpingExtent < JUMPING_LIMIT) {
                if (target.getY() > Main.scene.getHeightLimit())
                    target.setY(target.getY() - 10);
                else this.jumpingExtent = JUMPING_LIMIT;

            } else if (target.getY() + target.getHeight() < Main.scene.getFloorOffsetY()) {
                target.setY(target.getY() + 1);


            } else {
                target.setJumping(false);
                this.jumpingExtent = 0;
            }
        }
}
