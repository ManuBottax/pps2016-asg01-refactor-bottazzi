package model.characters.strategy;

import model.GameObject;
import model.characters.Character;


public interface HitStrategy {

    enum HitDirection {
        AHEAD,
        BACK,
        ABOVE,
        BELOW
    }

    boolean hit(Character object, GameObject target, HitDirection direction);
}
