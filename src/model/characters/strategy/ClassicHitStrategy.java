package model.characters.strategy;

import model.GameObject;
import model.characters.Character;

import java.lang.*;


public class ClassicHitStrategy implements HitStrategy {

    @Override
    public boolean hit(Character target, GameObject object, HitDirection direction) {
        boolean hit = false;
        switch(direction) {
            case AHEAD:
                if (target.isRightDirection()) {
                    hit = !(target.getX() + target.getWidth() < object.getX() || target.getX() + target.getWidth() > object.getX() + 5 ||
                            target.getY() + target.getHeight() <= object.getY() || target.getY() >= object.getY() + object.getHeight());
                } else {
                    return false;
                }
                break;

            case ABOVE:
                hit =  !(target.getX() + target.getWidth() < object.getX() + 5 || target.getX() > object.getX() + object.getWidth() - 5 ||
                        target.getY() < object.getY() + object.getHeight() || target.getY() > object.getY() + object.getHeight() + 5);
                break;

            case BACK:
                hit =  !(target.getX() > object.getX() + object.getWidth() || target.getX() + target.getWidth() < object.getX() + object.getWidth() - 5 ||
                        target.getY()+ target.getHeight() <= object.getY() || target.getY() >= object.getY() + object.getHeight());
                break;

            case BELOW:
                hit =  !(target.getX() + target.getWidth() < object.getX() + 5 || target.getX() > object.getX() + object.getWidth() - 5 ||
                        target.getY() + target.getHeight() < object.getY() || (target.getY() + target.getHeight()) > object.getY() + 5);
                break;
        }

        return hit;
    }
}
