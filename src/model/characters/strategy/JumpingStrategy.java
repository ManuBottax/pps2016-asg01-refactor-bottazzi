package model.characters.strategy;

import model.characters.Mario;

public interface JumpingStrategy {

    void performJump( Mario target);
}
