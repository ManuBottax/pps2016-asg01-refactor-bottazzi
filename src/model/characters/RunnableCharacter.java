package model.characters;

import model.GameObject;
import model.characters.strategy.HitStrategy;

import java.awt.*;


public abstract class RunnableCharacter extends BasicCharacter implements Runnable {

    private static final int PAUSE = 15;

    private Image characterImage;
    private int characterDirection;

    public RunnableCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.characterDirection = RIGHT_DIRECTION;
        this.setMoving(true);
    }

    public void setCharacterImage (Image image) {
        this.characterImage = image;
    }

    @Override
    public void move() {
            this.characterDirection = isRightDirection() ? RIGHT_DIRECTION : LEFT_DIRECTION;
            super.setX(super.getX() + this.characterDirection);
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject object) {
        if (this.hit(object, HitStrategy.HitDirection.AHEAD) && this.isRightDirection()) {
            this.setRightDirection(false);
            this.characterDirection = LEFT_DIRECTION;
        } else if (this.hit(object, HitStrategy.HitDirection.BACK) && !this.isRightDirection()) {
            this.setRightDirection(true);
            this.characterDirection = RIGHT_DIRECTION;
        }
    }

    public abstract Image getDeadImage();
}
