package model.characters;

import java.awt.Image;
import java.lang.*;

import game.Main;
import model.characters.strategy.ClassicJumpStrategy;
import model.characters.strategy.HitStrategy;
import model.characters.strategy.JumpingStrategy;
import model.objects.GameElement;
import model.objects.Piece;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;


    private Image marioImage;
    private boolean jumping;


    private JumpingStrategy jumpingStrategy;


    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.marioImage = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        jumpingStrategy = new ClassicJumpStrategy();
        //jumpingStrategy = new SuperJumpStrategy();
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    private void performJump(){

        jumpingStrategy.performJump(this);

    }

    public Image jump() {
        String path;

        performJump();

        path = this.isRightDirection() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        return Utils.getImage(path);
    }


    public void contact(GameElement object) {

        if (this.hit(object, HitStrategy.HitDirection.AHEAD) && this.isRightDirection() || this.hit(object, HitStrategy.HitDirection.BACK) && !this.isRightDirection()) {
            Main.scene.setMovement(0);
            this.setMoving(false);
        }

        if (this.hit(object, HitStrategy.HitDirection.BELOW) && this.jumping) {
            Main.scene.setFloorOffsetY(object.getY());
        } else if (!this.hit(object, HitStrategy.HitDirection.BELOW)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (this.hit(object, HitStrategy.HitDirection.ABOVE)) {
                Main.scene.setHeightLimit(object.getY() + object.getHeight());
            } else if (!this.hit(object, HitStrategy.HitDirection.ABOVE) && !this.jumping) {
                Main.scene.setHeightLimit(0);
            }
        }
    }

    public void hitEnemy(BasicCharacter character) {
        if (this.hit(character, HitStrategy.HitDirection.AHEAD) || this.hit(character, HitStrategy.HitDirection.BACK)) {
            if (character.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else this.setAlive(true);
        } else if (this.hit(character, HitStrategy.HitDirection.BELOW)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }

    public boolean contactPiece(GameElement piece) {
        if (piece instanceof Piece)
            return (this.hit(piece, HitStrategy.HitDirection.BACK) || this.hit(piece, HitStrategy.HitDirection.ABOVE) || this.hit(piece, HitStrategy.HitDirection.AHEAD)) || this.hit(piece, HitStrategy.HitDirection.BELOW);
        else
            return false;
    }


}
