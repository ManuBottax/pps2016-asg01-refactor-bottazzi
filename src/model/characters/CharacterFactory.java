package model.characters;


public interface CharacterFactory {

    RunnableCharacter createMushroom(int xPosition, int yPosition);
    RunnableCharacter createTurtle(int xPosition, int yPosition);
    Mario createMario(int xPosition, int yPosition);

}
