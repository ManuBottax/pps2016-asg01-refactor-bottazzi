package model.characters;

import java.awt.Image;

import game.Main;
import model.GameObject;
import model.characters.strategy.ClassicHitStrategy;
import model.characters.strategy.HitStrategy;
import utils.Res;
import utils.Utils;

public class BasicCharacter implements Character {

    public static final int RIGHT_DIRECTION = 1;
    public static final int LEFT_DIRECTION = -1;
    private static final int PROXIMITY_MARGIN = 10;

    private int width, height;
    private int x, y;
    private boolean isMoving;
    private boolean isRightDirection;
    private int counter;
    private boolean alive;
    private HitStrategy hitStrategy;



    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.isMoving = false;
        this.isRightDirection = true;
        this.alive = true;
        hitStrategy = new ClassicHitStrategy();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public boolean isRightDirection() {
        return isRightDirection;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.isMoving = moving;
    }

    public void setRightDirection(boolean rightDirection) {
        this.isRightDirection = rightDirection;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.isMoving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.isRightDirection ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public void move() {
        if (Main.scene.getXPos() >= 0) {
            this.x = this.x - Main.scene.getMovement();
        }
    }

    public boolean hit(GameObject object, HitStrategy.HitDirection direction){

        return hitStrategy.hit(this, object, direction);
    }

    public boolean isNearby(GameObject object) {
        return ((this.x > object.getX() - PROXIMITY_MARGIN && this.x < object.getX() + object.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > object.getX() - PROXIMITY_MARGIN && this.x + this.width < object.getX() + object.getWidth() + PROXIMITY_MARGIN));
    }
}
