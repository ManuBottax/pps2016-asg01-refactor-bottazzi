package model.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Turtle extends RunnableCharacter  {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        this.setCharacterImage(Utils.getImage(Res.IMG_TURTLE_IDLE));

        Thread turtle = new Thread(this);
        turtle.start();
    }


    public Image getDeadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
