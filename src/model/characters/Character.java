package model.characters;

import model.GameObject;

import java.awt.Image;

public interface Character extends GameObject{

    Image walk(String name, int frequency);

    int getCounter();

    boolean isAlive();

    boolean isMoving();

    boolean isRightDirection();

    boolean isNearby(GameObject object);


}
