package model.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Mushroom extends RunnableCharacter implements Runnable {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setCharacterImage(Utils.getImage(Res.IMG_MUSHROOM_DEFAULT));

        Thread mushroom = new Thread(this);
        mushroom.start();
    }


    public Image getDeadImage() {
        return Utils.getImage(this.isRightDirection() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
