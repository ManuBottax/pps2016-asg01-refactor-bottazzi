package model.objects;

import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Piece extends BaseElement implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;

    private int counter = 0;

    public Piece(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setObjectImage(Utils.getImage(Res.IMG_PIECE1));
    }

    @Override
    public Image getObjectImage() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.getObjectImage();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }
    }

}
