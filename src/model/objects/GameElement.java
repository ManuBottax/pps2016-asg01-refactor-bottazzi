package model.objects;

import model.GameObject;

import java.awt.*;

public interface GameElement extends GameObject{

    Image getObjectImage();
    void setObjectImage(Image image);



}
