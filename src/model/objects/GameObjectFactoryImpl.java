package model.objects;

import model.GameObject;

/**
 * Created by ManuBottax on 19/03/2017.
 */
public class GameObjectFactoryImpl implements GameObjectFactory {

    @Override
    public GameElement createBlock(int xPosition, int yPosition) {
        return new Block(xPosition,yPosition);
    }

    @Override
    public GameElement createTunnel(int xPosition, int yPosition) {
        return new Tunnel(xPosition,yPosition);
    }

    @Override
    public GameElement createPiece(int xPosition, int yPosition) {
        return new Piece(xPosition,yPosition);
    }
}
