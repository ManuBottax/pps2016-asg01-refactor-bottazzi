package model.objects;

import model.GameObject;

public interface GameObjectFactory {

    GameElement createBlock(int xPosition, int yPosition);
    GameElement createTunnel(int xPosition, int yPosition);
    GameElement createPiece(int xPosition, int yPosition);

}
