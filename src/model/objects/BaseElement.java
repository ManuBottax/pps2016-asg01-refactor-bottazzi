package model.objects;

import java.awt.Image;

import game.Main;

public class BaseElement implements GameElement {

    private int width, height;
    private int x, y;

    private Image objectImage;

    public BaseElement(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getObjectImage() {
        return this.objectImage;
    }

    public void setObjectImage(Image image){
        this.objectImage = image;
    }


    public void move() {
        if (Main.scene.getXPos() >= 0) {
            this.x = this.x - Main.scene.getMovement();
        }
    }

}
